#include <iostream>

#include <QApplication>
#include <QPushButton>


int main(int argc, char *argv[]) {

    QApplication app(argc, argv);

    QPushButton *button = new QPushButton("Hello, World!", NULL);
    button->show();
    button->raise();

    return app.exec();

}
